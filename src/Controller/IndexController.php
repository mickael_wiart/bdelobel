<?php

namespace App\Controller;

use DateTime;
use App\Entity\Personnes;
use App\Form\PersonnesType;
use App\Repository\PersonnesRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class IndexController extends AbstractController
{
    /**
     * @Route("/", name="index")
     */
    public function index(PersonnesRepository $repoPersonnes)
    {
        $personnes = new Personnes();
        $personnes = $repoPersonnes->findAll();
       

        return $this->render('index/index.html.twig', [
            'personnes' => $personnes,
        ]);
    }

    /**
     * @Route("/add", name="add_personne")
     * @param Request $request
     * @return Response
     */
    public function addPersonne(Request $request): Response
    {
        $personnes = new Personnes();
        $form = $this->createForm(PersonnesType::class, $personnes);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            
            $em = $this->getDoctrine()->getManager();
            $em->persist($personnes);
            $em->flush();
            $this->addFlash('success', 'Personne Ajouter');
            return $this->redirectToRoute('index');
        }

        return $this->render('index/add.html.twig', [
            'form' => $form->createView(),
        ]);
    }

   /**
     * @Route("/edit/{id}", name="edit_personne")
     */
    public function editPersonne(Request $request, PersonnesRepository $repoPersonne, ManagerRegistry $manager, $id)
    {
        $personne = new Personnes();
        $personne = $repoPersonne->find($id);
        $form = $this->createForm(PersonnesType::class, $personne);


        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            // Hash du mot de passe et roles par default
           
            $em = $manager->getManager();
            $em->persist($personne);
            $em->flush();
            $this->addFlash('success', 'Personne mise à jour avec succées !!!');
            return $this->redirectToRoute('index');
        }
        return $this->render('index/add.html.twig', [
            'form' => $form->createView()
        ]);

        
    }

    /**
     * @Route("/delete/{id}", name="remove_personne")
     */
    public function deletePersonne(PersonnesRepository $repoPersonne, $id)
    {
        $personne = new Personnes();
        $personne = $repoPersonne->find($id);

        $em = $this->getDoctrine()->getManager();
        $em->remove($personne);
        $em->flush();
       
        return $this->redirectToRoute('index'); 
    }

    /**
     * @Route("/view/{id}", name="view_personne")
     */
    public function viewPersonne(PersonnesRepository $repoPersonnes, $id)
    {
        $personne = new Personnes();
        $personne = $repoPersonnes->find($id);
        return $this->render('index/view.html.twig', [
            'personne' => $personne,
        ]);
    }
}
